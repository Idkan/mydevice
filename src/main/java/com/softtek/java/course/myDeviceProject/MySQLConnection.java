package com.softtek.java.course.myDeviceProject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLConnection {

	private final String driver;
    private final String url;
    private final String user;
    private final String password;
    public String SQL_SELECT = "Select * from DEVICE";

    private Connection connection;

    public MySQLConnection() {
    	
        // Database Parameters
    	driver 			= "com.mysql.cj.jdbc.Driver";
    	url				= "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#";
        user            = "root";
        password        = "1234";

        connection = null;
    }
    

    public void getConnection() {
        if(connection != null) {
            System.out.println("Connection already established with db");
            return;
        }

        try {
            Class.forName(driver).newInstance();
            this.connection = (Connection)DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the database!");
            
        } catch (ClassNotFoundException e) {
            System.out.println("Connection FAILED");
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public List<Device> getDevice() {
    	 PreparedStatement preparedStatement = null;
    	 List<Device> listDevice = new ArrayList<>();
    	 
    	 try {
    		 preparedStatement = connection.prepareStatement(SQL_SELECT);
    		 System.out.println("Query execute: " + SQL_SELECT);
    		 
    		 ResultSet resultSet = preparedStatement.executeQuery();
    		 
    		 while (resultSet.next()) { //iterable
    			 long deviceId = resultSet.getLong("DEVICEID");
                 String name = resultSet.getString("NAME");
                 String description = resultSet.getString("DESCRIPTION");
                 long manufacturerId = resultSet.getLong("MANUFACTURERID");
                 long colorId = resultSet.getLong("COLORID");
                 String comments = resultSet.getString("COMMENTS");

                 Device newDevice = new Device(deviceId,name,description,manufacturerId,colorId,comments);
                 listDevice.add(newDevice);
                 
    		 }
    		 
		} catch (Exception e) {
			// TODO: handle exception
		}
         
    	
		return listDevice;
    	
    }
}