package com.softtek.java.course.myDeviceProject;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Device {
	
	private Long deviceId;
	private String name;
	private String description;
	private Long manufacturerId;
	private Long colorId;
	private String comments;
	
	public Device() {}
	
	/**
	 * @param deviceId
	 * @param name
	 * @param description
	 * @param manufacturerId
	 * @param colorId
	 * @param comments
	 */
	public Device(Long deviceId, String name, String description, Long manufacturerId, Long colorId, String comments) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.description = description;
		this.manufacturerId = manufacturerId;
		this.colorId = colorId;
		this.comments = comments;
	}
	
	public Long getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public Long getColorId() {
		return colorId;
	}
	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}
