package com.softtek.java.course.myDeviceProject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	MySQLConnection conn = new MySQLConnection();
    	conn.getConnection();
    	
    	List<Device> resultDeviceList = new ArrayList<>();
    	resultDeviceList = conn.getDevice();
    	
    	System.out.println("----- Singleton pattern  -----");
    	for(Device deviceList : resultDeviceList) {
    		 System.out.println("DeviceId: " + deviceList.getDeviceId() + " Name: " + deviceList.getName() + 
            		 " Description: " + deviceList.getDescription() + " Manufacturer Id: " + 
    				 deviceList.getManufacturerId() + " Color Id: " + deviceList.getColorId() + 
    				 " Comments: " + deviceList.getComments());
    	}
    	
    	System.out.println("----- Map Device List into String list -----");
    	List<String> mapList = resultDeviceList.stream()
        	 	.map(Device::getName)
        	 	.collect(Collectors.toList());
    	mapList.forEach(System.out::println);
    	
    	System.out.println("----- Filter by Laptos -----");
    	List<String> resultFilter = resultDeviceList.stream()
    			.map(Device::getName)
    			.filter(name -> "Laptop".equals(name))
    		    .peek(System.out::println)
    			.collect(Collectors.toList());
    	
    	System.out.println("----- Count the number of devices where the manufacturer id is 3 -----");	
    	long count = resultDeviceList.stream()
    			.filter(c -> c.getManufacturerId() == 3 )
    			.count();
    	System.out.println(count);
    	
    	System.out.println("----- Get all devices as a List<Device> where the colorId = 1 -----");
		resultDeviceList.stream()
    			.filter(colorid -> colorid.getColorId() == 1)
    			.forEach(System.out::println);
		
		System.out.println("----- Map Using key and value -----");
        Map<Long, List<Device>> map = resultDeviceList.stream()
      		  .collect(Collectors.groupingBy(Device::getDeviceId));
        
        map.forEach((k,v) -> {
      	  System.out.println("Key: " + k + " Value: " + v);
        });

    }
}

